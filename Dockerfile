FROM node:latest AS build

ARG environnement=preprod

WORKDIR /app
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install
COPY *.json /app/
COPY src src
RUN npm run build -- --prod --build-optimizer -e $environnement

FROM nginx:alpine

COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/dist/* /usr/share/nginx/html/
COPY --from=build /app/src/assets /usr/share/nginx/html/assets
