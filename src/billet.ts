import {Participant} from './participant';
import {BilletOption} from './billet_option';

export class Billet {
  id: number;
  order: number;
  product: number;
  billet_option: Array<BilletOption>;

  participants:  Array<Participant>;

  constructor(billet) {
    console.log(billet);
    this.id = billet.id;
    this.order = billet.order;
    this.product = billet.product;
    this.participants = [];
    this.billet_option = [];
    for (const bo of billet.billet_options){
      this.billet_option.push(new BilletOption(bo));
    }
    for (const p of billet.participants)
    {
      this.participants.push(new Participant(p));
    }
  }

}
