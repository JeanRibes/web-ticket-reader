import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../app.component';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errors: string;
  username: string;
  password: string;
  file: number;


  constructor(private auth_service: AuthService, private router: Router, private cookie_service: CookieService) {
    this.router = router;

  }

  ngOnInit() {
  }

  submit() {
    this.auth_service.authenticate(this.username, this.password).subscribe(
      (token) => {
        AppComponent.JWT = token['token'];
        this.cookie_service.put('token', token['token']);
        this.router.navigate(['params']);
      },
      (error) => this.errors = 'Authentification échouée !'

    );


  }

}
