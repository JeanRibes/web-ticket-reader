export class Place {
  nom_table: string;
  id: number;

  constructor (place) {
    this.nom_table = place.table.nom_table;
    this.id = place.id;
  }
}
