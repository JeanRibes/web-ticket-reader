import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Billet} from '../../billet';
import {TicketValidatorService} from '../ticket-validator.service';
import {AppComponent} from '../app.component';

import {Place} from '../place';
import {CookieService} from 'angular2-cookie/core';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {


  /***L'ID du billet en cours de vérification
   *
   * @type {string}
   */
  id = '';
  /***
   * L'ID deu dernier billet scanné
   */
  last_id = '';
  /***Stocke les messages d'erreur
   *
   */
  errors: string;
  /**Le billet en cours de vérification, une fois qu'on sait qu'il existe
   *
   */
  billet: Billet;
  /***Stocke les informations
   *
   */
  info: string;
  /***
   * True = scanette
   * False = QR_code
   */
  mode: boolean;
  places: Place[];


  constructor(private ticket_validator: TicketValidatorService, private ref: ChangeDetectorRef, private cookie_service: CookieService) {
    this.mode = true;
    AppComponent.JWT = this.cookie_service.get('token');
    if (this.cookie_service.get('mode') === 'true') {
      AppComponent.mode_compostage = true;
    } else {
      AppComponent.mode_compostage = false;
    }
    AppComponent.file = parseInt(this.cookie_service.get('file'));

  }

  ngOnInit(): void {

  }

  /*** Fonction appelée lors d'un click sur le bouton envoyer, ou lors d'un scan avec la douchette
   *
   */
  onClick() {

    if (!AppComponent.JWT) {
      this.errors = 'Vous n\'êtes pas authentifié ! Allez sur la page de paramètre';
      document.body.style.background = 'red';
      return;
    }


    // On demande d'abord au service si le billet existe
    this.ticket_validator.checkBillet(this.id).subscribe(
      (billet) => {
        // Si il y a un billet, on parse les données et on le composte
        this.billet = new Billet(billet);

        if (AppComponent.mode_compostage) {
          this.composterbillet();
        } else {
          this.recupererplaces();
        }


        this.errors = '';
        this.info = '';
        this.ref.detectChanges();

      },
      (err) => {
        // Si il y a une erreur, c'est que le ticket n'est pas reconnu
        this.errors = 'Ticket non reconnu ! ';
        this.billet = null;
        this.ref.detectChanges();
        document.body.style.background = 'red';

      }
    );
    this.last_id = this.id;
    this.id = '';
    this.ref.detectChanges();
    setTimeout(this.go_white, 1000);


  }

  composterbillet() {
    this.ticket_validator.composterBillet(this.billet).subscribe(
      // Si les données renvoyées sont une suite de caractère => y'a une erreur !
      (compostage) => {

        this.info = 'Billet Validé !';
        if (this.billet.product != null) {
          if (this.billet.product === 1) {
            document.body.style.background = 'pink';
          } else {
            document.body.style.background = 'green';
          }

        } else {

          document.body.style.background = 'yellow';
        }

        this.ref.detectChanges();
      }, (errors) => {
        console.log(errors);
        this.errors = errors.error;
        document.body.style.background = 'red';
        this.billet = null;
        this.ref.detectChanges();
      }
    );
  }

  recupererplaces() {
    this.ticket_validator.getPlaces(this.billet).subscribe(
      (places) => {
        this.info = '';
        this.places = places;

      }
    );

  }

  switchmode() {
    this.mode = !this.mode;
    console.log(this.mode);

  }

  go_white() {
    document.body.style.background = 'white';


  }

  decodedOutput(event) {
    this.errors = '';
    console.log(event);
    this.id = event;
    this.onClick();
    this.ref.detectChanges();


  }


}
