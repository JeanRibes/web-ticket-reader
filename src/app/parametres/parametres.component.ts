import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {AppComponent} from '../app.component';
import {Router} from '@angular/router';
import {TicketValidatorService} from '../ticket-validator.service';
import {File} from '../file';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-parametres',
  templateUrl: './parametres.component.html',
  styleUrls: ['./parametres.component.css']
})
export class ParametresComponent implements OnInit {


  errors: string;
  file: number;
  files: File[];

  constructor(private auth_service: AuthService, private ticket_service: TicketValidatorService,
              private router: Router, private cookie: CookieService) {
    this.router = router;
    ticket_service.getFiles().subscribe(files => this.files = files);

  }

  ngOnInit() {
  }

  submit() {
          if (this.file < 1) {
            alert('Le numéro de file est invalide');
          }
          AppComponent.file = this.file;
          this.cookie.put('file', this.file.toString());
          for (const file of this.files){
            if (file.id == this.file) {

              if (file.validation_type == 3) {
                AppComponent.mode_compostage = false;
                this.cookie.put('mode', 'false');
              }else {
                AppComponent.mode_compostage = true;
                this.cookie.put('mode', 'true');
              }

            }
           }

          this.router.navigate(['']);
  }

}
